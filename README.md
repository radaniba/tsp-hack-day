#Targeted Sequencing Pipeline Starter Guide

##Introduction

In this tutorial we will go through all the steps required to run a targeted sequencing analysis using the pipeline factory. No coding experience is needed if you follow this tutorial

There are 4 modes to use for the targeted sequencing pipeline

- Classic version (simplest one, you know the positions that were called using a third party tool)
- Discovery mode with GATK (You know only the amplicon starts and ends, we use GATK to call variants)
- Dicovery mode with mutationSeq (You know only the amplicon starts and ends, we use mutationSeq to call variants, no normal sample specified)
- Discovery mode with mutationSeq (You know only the amplicon starts and ends, we use mutationSeq to call variants in paired mode normal vs tumour)

Each version of the pipeline is usable for specific situations. We created this simple decision tree to help you figure out which version you should go with:

![decision_tree](https://svn.bcgsc.ca/stash/users/raniba/repos/targeted_sequencing_tutorial/browse/img/decision_tree/decision_tree.001.png?at=827683fb0d825764f70df081fe743d7e3e44fc44&raw)

In this Tutorial we will be using only the classic version for simplicity's sake.

In this course we will go through these steps :

- Set up the working environment on Genesis
- Set up working directories for easy tracking
- Loading the virtual environment for the targeted sequencing pipeline
- Preparing the data 
- Running the pipeline
- Checking the run status
- Checking the results

This course is easy to follow and to reproduce, if you have any question please ask Rad or email at raniba@bccrc.ca

Let's get started ...


##Before You Start


Before we start the analysis, we need to login at gsc , this is done via ssh usually 

``` 
ssh username@ssh.bcgsc.ca 
```

Use your credentials to authenticate. 

Once logged in, we need to load every path and environment setup to run the pipeline, these are usually paths to libraries and dependencies needed for the pipeline to run. 

Luckily, these are all included in what we call a virtual environment specific for the pipeline, all you have to do is run on single command wich is 

```
source /genesis/extscratch/shahlab/pipelines/virtual_environments/targeted_sequencing_pipeline/set_env.sh
```

![copy_image_url](https://bytebucket.org/radaniba/tsp-hack-day/raw/2a75d4473fe592fb03842dd07175289d1cf17c1f/img/load_virtual_env.gif?token=aeabf8ecb8184a3bedf4f17fd124025f044d35d6)

This will change your default environment and you should see `(targeted_sequencing_pipeline)` added before prompt, this is normal and this is a good sign that we are now working under the TSP environment


##Make sure you are running the right python version


By loading the TSP environment, you are running a specific version of python loaded with the necessary libraries and packages. To make sure you're running the appropriate python version type this command in your terminal (as shown in the figure), you should see the same result

![check_python](https://bytebucket.org/radaniba/tsp-hack-day/raw/2a75d4473fe592fb03842dd07175289d1cf17c1f/img/check_python.gif?token=fc8bec55436b642fdd5e713b66f62bede087786c)


##Create a working directory


In this example we will create a directory called `TSP` and a subdirectory called `experiment`

`TSP` will contain some files we will describe in the next section
`experiment` will contain all the output for a specific experiment, you can name it whatever you want to easily track your results

![working_env](https://bytebucket.org/radaniba/tsp-hack-day/raw/2a75d4473fe592fb03842dd07175289d1cf17c1f/img/create_working_directory.gif?token=b6d3363d4862ea1fa024c7d3ad5020c3842b3782) 


##Files we need


In order to run the pipeline we need to use the [pipeline runner](http://wiki.mo.bccrc.ca/display/lab/Pipeline+Runner) , please read the doc for details and/or ask Jamie

For our example we need 3 files

####A sample file

This is a *tab seperated* file containing the forward and reverse fastq files as well as the sample ids (**you must keep the header line**)

```
#sample_id  fastq_file_1    fastq_file_2    sample_id
Sample10    /extscratch/shahlab/raniba/pipelines/miseq_pipeline/miseq_analysis_pipeline/fastq/A21Y0/Sample10_S10_L001_R1_001.fastq  /extscratch/shahlab/raniba/pipelines/miseq_pip
eline/miseq_analysis_pipeline/fastq/A21Y0/Sample10_S10_L001_R2_001.fastq    Sample10
Sample11    /extscratch/shahlab/raniba/pipelines/miseq_pipeline/miseq_analysis_pipeline/fastq/A21Y0/Sample11_S11_L001_R1_001.fastq  /extscratch/shahlab/raniba/pipelines/miseq_pip
eline/miseq_analysis_pipeline/fastq/A21Y0/Sample11_S11_L001_R2_001.fastq    Sample11
```

####A targets file (aka Manifest file)

This file should contain the targets that you are interested in and the following is an example of the content of this file :

```
chrom   coord   ref_base    var_base    amplicon_beg    amplicon_end
1   103483514   C   T   103483453   103483713
1   196659237   C   T   196659184   196659408
1   28209366    A   G   28209333    28209569
10  3178881 C   T   3178834 3179060
10  38894616    C   T   38894575    38894800
11  104972190   C   T   104972163   104972431
11  112584001   C   G   112583972   112584238
11  117800083   T   C   117800054   117800297
12  25957056    A   T   25957025    25957275

...
```

####A setup file

This is where all setting for the pipeline are set

```
__OPTIONS__ factory_dir /genesis/extscratch/shahlab/pipelines/factory/pipeline-factory/pipeline_factory/
__OPTIONS__ components_dir  /genesis/extscratch/shahlab/raniba/Tasks/component_dev/components/
__OPTIONS__ drmaa_library_path  /opt/sge/lib/lx24-amd64/libdrmaa.so
__GENERAL__ python  /genesis/extscratch/shahlab/pipelines/virtual_environments/targeted_sequencing_pipeline/bin/python
__GENERAL__ samtools    /genesis/extscratch/shahlab/pipelines/apps/samtools-1.2/samtools
__GENERAL__ bowtie2 /genesis/extscratch/shahlab/pipelines/apps/bowtie2-2.0.2/bowtie2
__SHARED__  variant_positions   'path_to_the_file_containing_the_targets'
__SHARED__  ref_genome  /genesis/extscratch/shahlab/pipelines/apps/bowtie2-2.0.2/genome/GRCh37-lite.fa_bt2-2.0.2
__SHARED__  ref_genome_fa   /genesis/extscratch/shahlab/pipelines/apps/reference/GRCh37-lite.fa
__SHARED__  memory  '5G'
__SHARED__  snv_calls   'This file needs to be  mentioned if you want an excel summary of the results'
__SHARED__  snv_calls_ids   None
__SHARED__  normal_id   None
```

You only need to add the path to the targets file specified in this line :

```
__SHARED__  variant_positions   'path_to_the_file_containing_the_targets'
```


##Running the pipeline

To run the pipeline we will be using the pipeline runner, it is a utility developed by the dev team (Jamie) to save you some time and to simplify further the pipeline factory usage

You need to go through [the runner documentation]((http://wiki.mo.bccrc.ca/display/lab/Pipeline+Runner)) to understand what it is doing

Save the previous samples file as `samples.txt` and the setup examples as `miseq.setup`

To lunch the pipeline you need to type this command

```
python /extscratch/shahlab/pipelines/factory/pipeline-tools/pipeline_runner/pipeline_runner.py --working_dir experiment/ --config /extscratch/shahlab/raniba/Tasks/component_dev/genesis_production_pipelines/pipelines/miseq_latest/miseq_classic_mode_pipeline_v2.0/miseq_classic_version_pipeline_v2.0.yaml --samples samples.txt --setup miseq.setup --run_id EXPERIMENT_ID
```

![run_pipeline](https://bytebucket.org/radaniba/tsp-hack-day/raw/2a75d4473fe592fb03842dd07175289d1cf17c1f/img/run_pipeline.gif?token=e2054a5bff97be9ffa79da7dc06f78e803e67d00)


##Checking the results

All the results will be stored in the subdirectory you specified in your command line as `EXPERIMENT_ID` 

Each sample will have a dedicated subdirectory, in which every step of the pipeline analysis will output results in directories such as :


```
Sample_Name/
|-- logs
|-- outputs
|   `-- results
|       |-- bam
|       |-- counts
|       |-- positions
|       `-- variant_status
|-- scripts
`-- sentinels
```

where : 

``` 
logs : directory with logging for all stages of the analysis 
outputs/results/bam : where all the alignments for this sample are located
outputs/results/counts : where all counts for the background calculation are done 
outputs/results/positions : background positions for the binomial test 
outputs/results/variant_status : final result for the positions specified (the most important file to look at)
``` 

##While the pipeline is running on gsc 

You will be probably running the pipeline with other people on the cluster, to verify the status of your pipeline analysis you can check this by using : 

```
qstat -u <your_username>
```

##Contact us 

To report a problem or just to say hi : contact Rad at raniba@bccrc.ca 


##Survey

If you have a few second we would like to [collect your opinion](https://docs.google.com/forms/d/1m9sA_FpETIKnZxAlGR4mHyR7r2qcKbJGxxx9bbAAxiM/viewform)